ARG PHP_VERSION=7.4.27
ARG COMPOSER_VERSION=2.2.3

FROM composer:$COMPOSER_VERSION as composer-image
FROM php:${PHP_VERSION}-fpm-alpine

ARG REDIS_VERSION=5.3.5
ARG APCU_VERSION=5.1.21

# configure image
RUN apk add --no-cache $PHPIZE_DEPS \
        bash \
        autoconf \
        libintl \
        icu icu-dev \
        libxml2-dev \
        curl-dev \
        libsodium-dev \
        imap-dev \
        freetype-dev libjpeg-turbo-dev libmcrypt-dev libpng-dev \
        libzip-dev \
    # redis
    && pecl install redis-${REDIS_VERSION} \
    && docker-php-ext-enable redis \
    # apcu
    && pecl install apcu-${APCU_VERSION} \
    && docker-php-ext-enable apcu \
    # mysqli, pdo_mysql, pcntl, intl, zip
    && docker-php-ext-install mysqli pdo_mysql pcntl intl zip \
    # imap
    && docker-php-ext-configure imap --with-imap --with-imap-ssl \
    && docker-php-ext-install imap \
    # gd
    && docker-php-ext-configure gd --with-jpeg --with-freetype \
    && docker-php-ext-install gd \
    && rm -rf /var/cache/apk/*

# scripts
COPY ./keep-alive.sh /scripts/keep-alive.sh
COPY ./scheduler.sh /scripts/scheduler.sh
COPY ./fpm-entrypoint.sh /fpm-entrypoint.sh

# configs
COPY ./php-fpm.conf /usr/local/etc/php-fpm.conf
COPY ./php.ini /usr/local/etc/php/conf.d/php.ini
COPY ./opcache.ini /usr/local/etc/php/conf.d/opcache.ini

# install composer
COPY --from=composer-image /usr/bin/composer /usr/local/bin/composer

# prepare workdir
RUN mkdir -p /app \
    && addgroup \
        --gid 1000 \
        webuser \
    && adduser \
        --disabled-password \
        --gecos "" \
        --ingroup "webuser" \
        --uid 1000 \
        --no-create-home \
        webuser \
    && chown webuser:webuser -R /app

RUN set -xe \
    php -i

WORKDIR /app
ENTRYPOINT []
CMD []
